#!/bin/bash -ex

if [ -f .secrets ]; then
	source .secrets
else
	echo "Secrets not Found!"
	exit 1
fi

TARGET=$1

## local merge config files for environments
go run ./mergeConfigs.go "${TARGET}"
envsubst < etc/gitea/conf/app.ini > etc/gitea/conf/app.ini~
mv etc/gitea/conf/app.ini~ etc/gitea/conf/app.ini


## begin tmp file preparation
TMP=`mktemp -d /tmp/XXXXXXXXX`
cp -r etc ${TMP}

if [[ ${TARGET} != *production.* ]] ; then
	BANNER="
		<div style='background: #700; color: white; text-align: center; font-weight: 600; padding: .4em;' id='cb-test-warning'>
			WARNING: THIS IS A TEST INSTANCE. DATA CAN VANISH AT ANY TIME.
			<a onclick='document.getElementById(\"cb-test-warning\").remove()'>[x]</a>
		</div>"
	mkdir -p ${TMP}/etc/gitea/templates/custom/
	echo "${BANNER}" > ${TMP}/etc/gitea/templates/custom/body_outer_pre.tmpl
	echo -e "User-Agent: *\nDisallow: /" > ${TMP}/etc/gitea/robots.txt
fi

# begin remote deployment
rsync -av -e ssh --delete --chown=git:git ${TMP}/etc/gitea root@${TARGET}:/etc/
rm -rf ${TMP}

ssh root@${TARGET} /bin/bash << EOF
chown git:git /etc/gitea -R
mkdir -p /data/git/log
chown git:git /data/git/bin -R

# Stop services and put new Gitea in place.
systemctl daemon-reload
systemctl stop gitea
mv /data/git/bin/gitea /data/git/bin/gitea.backup
mv /data/git/bin/gitea.new /data/git/bin/gitea

sed -i 's/^.*requirepass .*/requirepass ${REDIS_PWD}/g' /etc/redis/redis.conf

# Bring all services back online.
systemctl start gitea
systemctl status gitea
EOF



