package main

import (
	"os"

	"gopkg.in/ini.v1"
)

const (
	configPrefix = "etc/gitea/conf/"
)

func main() {
	target := os.Args[1]
	iniFile, err := ini.Load(configPrefix + "base.ini", configPrefix + target + ".ini")
	if err != nil {
		panic(err)
	}
	err = iniFile.SaveTo(configPrefix + "app.ini")
	if err != nil {
		panic(err)
	}
}

